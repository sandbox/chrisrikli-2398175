<?php

function fdfw_admin_settings_form() {
  $form = array();


  $form['fdfw_url'] = array(
    '#title' => t('Freshdesk Support URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('fdfw_url',''),
    '#required' => TRUE,
  );

  $form['button'] = array(
    '#type' => 'fieldset',
    '#title' => t('Customize Button'),
    '#collapsible' => TRUE,
  );

  $form['button']['fdfw_btn_type'] = array(
    '#type' => 'radios',
    '#title' => 'Button Type',
    '#options' => array(
      'txt' => 'Text button',
      'img' => 'Image button',
    ),
    '#default_value' => variable_get('fdfw_btn_type', 'txt'),
  );

  $form['button']['fdfw_btn_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#size' => 15,
    '#default_value' => variable_get('fdfw_btn_label', 'Support'),
    '#required' => TRUE,
  );

  $form['button']['fdfw_btn_txt_color'] = array(
    '#type' => 'select',
    '#title' => t('Text Color'),
    '#options' => array(
      'white' => t('White'),
      'black' => t('Black'),
    ),
    '#default_value' => variable_get('fdfw_btn_txt_color', 'white'),
    '#required' => TRUE,
  );

  $form['button']['fdfw_btn_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Color'),
    '#size' => 7,
    '#default_value' => variable_get('fdfw_btn_bg_color', '#8F0E1A'),
    '#required' => TRUE,
  );

  $form['button']['fdfw_btn_alignment'] = array(
    '#type' => 'select',
    '#title' => t('Widget Position'),
    '#description' => t('Choose widget position on your browser.'),
    '#options' => array(
      4 => t('Left'),
      2 => t('Right'),
      1 => t('Top'),
      3 => t('Bottom'),
    ),
    '#default_value' => variable_get('fdfw_btn_alignment', 4),
    '#required' => TRUE,
  );

  $form['button']['fdfw_btn_offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Offset'),
    '#description' => t('The offset will be the distance of the widget button (pixels) from the left or top of the screen based on the alignment you choose.'),
    '#field_suffix' => 'px',
    '#size' => 6,
    '#default_value' => variable_get('fdfw_btn_offset', 235),
    '#required' => TRUE,
  );

  $form['form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Customize Form'),
    '#collapsible' => TRUE,
  );

  $form['form']['fdfw_form_heading'] = array(
    '#type' => 'textfield',
    '#title' => t('Form Heading'),
    '#default_value' => variable_get('fdfw_form_heading', 'Help & Support'),
    '#required' => TRUE,
  );

  $form['form']['fdfw_form_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Form Submit Message'),
    '#description' => t('Message to be displayed after a user submits the form'),
    '#default_value' => variable_get('fdfw_form_message', 'Thank you for your feedback'),
    '#required' => TRUE,
  );

  $form['form']['fdfw_form_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Form Height'),
    '#field_suffix' => 'px',
    '#size' => 6,
    '#default_value' => variable_get('fdfw_form_height', 500),
    '#required' => TRUE,
  );

  $form['form']['fdfw_form_screenshot'] = array(
    '#type' => 'checkbox',
    '#title' => t('Take Screenshot'),
    '#default_value' => variable_get('fdfw_form_screenshot', 1),
  );

  $form['form']['fdfw_form_attach'] = array(
    '#type' => 'checkbox',
    '#title' => t('Attach a file'),
    '#default_value' => variable_get('fdfw_form_attach', 1),
  );

  $form['form']['fdfw_form_search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Search Articles'),
    '#default_value' => variable_get('fdfw_form_search', 1),
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
  );

  $form['advanced']['fdfw_field_url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL Target Field',
    '#default_value' => variable_get('fdfw_field_url',''),
  );

  $desc = t('Requires that the !l module be enabled', array('!l' => l('Browscap', 'https://www.drupal.org/project/browscap', array('attributes' => array('target' => '_blank')))));
  $has_browscap = module_exists('browscap');
  $form['advanced']['fdfw_field_device'] = array(
    '#type' => 'textfield',
    '#title' => 'Device Target Field',
    '#description' => $desc,
    '#default_value' => variable_get('fdfw_field_device',''),
    '#disabled' => !$has_browscap,
  );

  $form['advanced']['fdfw_field_os'] = array(
    '#type' => 'textfield',
    '#title' => 'OS Target Field',
    '#description' => $desc,
    '#default_value' => variable_get('fdfw_field_os',''),
    '#disabled' => !$has_browscap,
  );

  $form['advanced']['fdfw_field_browser'] = array(
    '#type' => 'textfield',
    '#title' => 'Browser Target Field',
    '#description' => $desc,
    '#default_value' => variable_get('fdfw_field_browser',''),
    '#disabled' => !$has_browscap,
  );

  return system_settings_form($form);
}
